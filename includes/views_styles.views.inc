<?php
// Id:$

/**
 * @file Add Views module hooks to Views Styles.
 */
 
/**
 * Implementation of hook_views_plugins().
 */
function views_styles_views_plugins() {
  $path = drupal_get_path('module', 'views_styles');
  
  return array(
    'style' => array(
      'views_styles' => array(
        'title' => t('Tabs'),
        'help' => t('Display view in Tabs.'),
        'handler' => 'views_styles_style_plugin',
        'path' => "$path/includes",
        'theme' => 'views_styles_view',
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}

function theme_views_styles_views_render($view, $tabs, $qtid) {
  foreach ($tabs as $index => $tab) {
    if (is_array($tab['content'])) {
      $text = '';
      foreach ($tabs[$index]['content'] as $content) {
        $text .= '<div class="views_styles-views-group">' . $content . '</div>';
      }
      $tabs[$index]['text'] = $text;
    }
    else {
      $tabs[$index]['text'] = $tabs[$index]['content'];
    }
    $tabs[$index]['type'] = 'freetext';
    unset($tabs[$index]['content']);
  }

  $views_styles = array(
    'qtid' => $qtid,
    'ajax' => '0', // TODO: Support AJAX, set this to $view->use_ajax.
    'hide_empty_tabs' => '0',
    'default_tab' => '0',
    'style' => $view->style_options['tab_style'],
    'tabs' => $tabs,
  );

  return theme('views_styles', $views_styles);
}
