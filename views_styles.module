<?php

/**
 * Implementation of hook_theme().
 */
function views_styles_theme() {
  return array(
    'views_styles_views_render' => array(
      'arguments' => array('view' => NULL, 'tabs' => NULL, 'qtid' => NULL),
      'file' => 'includes/views_styles.views.inc',
    ),
    'views_styles_tabs' => array(
      'arguments' => array('views_styles', 'active_tab' => 'none'),
    ),
    'views_styles' => array(
      'arguments' => array('views_styles'),
    ),
    'views_styles_tab_access_denied' => array(
      'arguments' => array('tab'),
    ),
  );
}

/**
 * Implementation of hook_perm().
 */
function views_styles_perm() {
  return array('administer views_styles');
}

/**
 * Implementation of hook_block().
 */
function views_styles_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks = array();
      $result = db_query('SELECT qtid, title FROM {views_styles}');
      while ($row = db_fetch_object($result)) {
        $blocks[$row->qtid]['info'] = $row->title;
        $blocks[$row->qtid]['cache'] = BLOCK_NO_CACHE;
      }
      return $blocks;
      break;

    case 'view':
      $mainblock = array();
      if ($views_styles = views_styles_load($delta)) {
        $mainblock['subject'] = $views_styles['title'];
        $mainblock['content'] = theme('views_styles', $views_styles);
      }
      return $mainblock;
      break;
  }
}

/**
 * Render views_styles.
 */
function views_styles_render($views_styles) {
  // convert views arguments to an array, retrieving %-style args from url
  $views_styles['tabs'] = _views_styles_prepare_views_args($views_styles['tabs']);

  if ($views_styles['hide_empty_tabs'] && !$views_styles['ajax']) {
    // Remove empty tabpgages.
    foreach ($views_styles['tabs'] as $key => $tab) {
      $contents = views_styles_render_tabpage($tab, TRUE);
      if (empty($contents)) {
        unset($views_styles['tabs'][$key]);
      }
    }
    $views_styles['tabs'] = array_merge($views_styles['tabs']);
  }

  $tabs_count = count($views_styles['tabs']);
  if ($tabs_count <= 0) {
    return '';
  }

  if ($views_styles['style'] == 'default') {
    $views_styles['style'] = variable_get('views_styles_tabstyle', 'nostyle');
  }

  views_styles_add_css($views_styles['style']);
  $javascript = drupal_add_js('misc/progress.js', 'core');
  if (!isset($javascript['setting'][1]['views_styles']) || !array_key_exists('qt_'. $views_styles['qtid'], $javascript['setting'][1]['views_styles'])) {
    // Only the tabs are used in views_styles.js
    $settings = array(
      'tabs' => $views_styles['ajax'] ? $views_styles['tabs'] : views_styles_array_fill_keys(array_keys($views_styles['tabs']), 0),
    );
    drupal_add_js(array('views_styles' => array('qt_'. $views_styles['qtid'] => $settings)), 'setting');
  }
  drupal_add_js(drupal_get_path('module', 'views_styles') .'/js/views_styles.js');

  $attributes = drupal_attributes(array(
    'id' => 'views_styles-'. $views_styles['qtid'],
    'class' => 'views_styles_wrapper views_styles-style-'. drupal_strtolower($views_styles['style']),
  ));
  $output = '<div'. $attributes .'>';
  $active_tab = _views_styles_get_active_tab($views_styles);
  $output .= theme('views_styles_tabs', $views_styles, $active_tab);

  // The main content area, each quicktab container needs a unique id.
  $attributes = drupal_attributes(array(
    'id' => 'views_styles_container_'. $views_styles['qtid'],
    'class' => 'views_styles_main views_styles-style-'. drupal_strtolower($views_styles['style']),
  ));
  $output .= '<div'. $attributes .'>';

  if ($views_styles['ajax']) {
    // Prepare ajax views.
    _views_styles_prepare_views($views_styles['tabs']);
    // Render only the active tabpage.
    if (isset($active_tab)) {
      $attributes = drupal_attributes(array(
        'id' => 'views_styles_tabpage_'. $views_styles['qtid'] .'_'. $active_tab,
        'class' => 'views_styles_tabpage',
      ));
      $output .= '<div'. $attributes .'>'. views_styles_render_tabpage($views_styles['tabs'][$active_tab]) .'</div>';
    }
  }
  else {
    // Render all tabpgages.
    foreach ($views_styles['tabs'] as $key => $tab) {
      $attributes = drupal_attributes(array(
        'id' => 'views_styles_tabpage_'. $views_styles['qtid'] .'_'. $key,
        'class' => 'views_styles_tabpage'. ($active_tab == $key ? '' : ' views_styles-hide'),
      ));
      $output .= '<div'. $attributes .'>'. views_styles_render_tabpage($tab) .'</div>';
    }
  }

  $output .= '</div></div>';

  return $output;
}

/**
 * Ajax callback for tab content.
 */
function views_styles_ajax($type) {

  $args = func_get_args();
  $type = array_shift($args);
  $func = 'views_styles_ajax_'. $type;
  $output = call_user_func_array($func, $args);
  $js_css = views_styles_ajax_js_css();
  $response = array('content' => $output, 'js_css' => $js_css);
  drupal_json(array('status' => 0, 'data' => $response));
}

/**
 * Ajax callback for node tabpage.
 */
function views_styles_ajax_node($nid, $teaser, $hide_title) {
  $tabpage = array(
    'type' => 'node',
    'nid' => $nid,
    'teaser' => $teaser,
    'hide_title' => $hide_title,
  );
  $output = views_styles_render_tabpage($tabpage);
  return $output;
}

/**
 * Ajax callback for block tabpage.
 */
function views_styles_ajax_block($bid, $hide_title) {
  $tabpage = array(
    'type' => 'block',
    'bid' => $bid,
    'hide_title' => $hide_title,
  );

  $output = views_styles_render_tabpage($tabpage);
  return $output;
}

/**
 * Ajax callback for qtabs tabpage.
 */
function views_styles_ajax_qtabs($qtid) {
  $tabpage = array(
    'type' => 'qtabs',
    'qtid' => $qtid,
  );

  $output = views_styles_render_tabpage($tabpage);
  return $output;
}

/**
 * Theme function to display the access denied tab.
 *
 * @ingroup themeable
 */
function theme_views_styles_tab_access_denied($tab) {
  return t('You are not authorized to access this content.');
}

/**
 * Theme function to output views_styles.
 *
 * @ingroup themeable
 */
function theme_views_styles($views_styles) {
  return views_styles_render($views_styles);
}

/**
 * Theme function for output of the tabs. Use this to ADD extra classes.
 * The general structure 'ul.views_styles_tabs li a' needs to be maintained
 * for the jQuery to work.
 *
 * @ingroup themeable
 */
function theme_views_styles_tabs($views_styles, $active_tab = 'none') {
  $output = '';
  $tabs_count = count($views_styles['tabs']);
  if ($tabs_count <= 0) {
    return $output;
  }

  $index = 1;
  $output .= '<ul class="views_styles_tabs views_styles-style-'. drupal_strtolower($views_styles['style']) .'">';
  foreach ($views_styles['tabs'] as $tabkey => $tab) {
    $class = 'qtab-'. $tabkey;
    // Add first, last and active classes to the list of tabs to help out themers.
    $class .= ($tabkey == $active_tab ? ' active' : '');
    $class .= ($index == 1 ? ' first' : '');
    $class .= ($index == $tabs_count ? ' last': '');
    $attributes_li = drupal_attributes(array('class' => $class));
    $options = _views_styles_construct_link_options($views_styles, $tabkey);
    // Support for translatable tab titles with i18nstrings.module.
    if (module_exists('i18nstrings')) {
      $tab['title'] = tt("views_styles:tab:$views_styles[qtid]--$tabkey:title", $tab['title']);
    }
    $output .= '<li'. $attributes_li .'>'. l($tab['title'], $_GET['q'], $options) .'</li>';
    $index++;
  }
  $output .= '</ul>';
  return $output;
}

/**
 * Helper function to construct link options for tab links.
 */
function _views_styles_construct_link_options($views_styles, $tabkey) {
  $qtid = $views_styles['qtid'];
  $ajax = $views_styles['ajax'];
  $tab = $views_styles['tabs'][$tabkey];
  $id = 'views_styles-tab-'. implode('-', array($qtid, $tabkey));

  // Need to construct the correct query for the tab links.
  $query = $_GET;
  unset($query['views_styles_'. $qtid]);
  unset($query['q']);
  unset($query['page']);
  $query['views_styles_'. $qtid] = $tabkey;

  if ($ajax) {
    $class = 'qt_ajax_tab';
  }
  else {
    $class = 'qt_tab';
  }

  $link_options = array(
    'attributes' => array(
      'id' => $id,
      'class' => $class,
    ),
    'query' => $query,
    'fragment' => 'views_styles-'. $qtid,
  );

  return $link_options;
}

/**
 * Fetch the necessary CSS files for the tab styles.
 */
function views_styles_add_css($style) {
  // Add views_styles CSS.
  drupal_add_css(drupal_get_path('module', 'views_styles') .'/css/views_styles.css');

  if ($style == 'default') {
    // Get the default style.
    $style = variable_get('views_styles_tabstyle', 'nostyle');
  }

  $style_css = _views_styles_get_style_css($style);

  if ($style_css != 'nostyle') {
    drupal_add_css($style_css, 'module');
  }
}

/**
 * Helper function to get the css file for given style.
 */
function _views_styles_get_style_css($style = 'nostyle') {
  static $tabstyles;

  if ($style != 'nostyle') {
    if (!isset($tabstyles)) {
      $tabstyles = module_invoke_all('views_styles_tabstyles');
    }
    foreach ($tabstyles as $css_file => $tabstyle) {
      if ($style == $tabstyle) {
        return $css_file;
      }
    }
  }

  return 'nostyle';
}

/**
 * Implementation of hook_views_styles_tabstyles().
 *
 * This hook allows other modules to create additional tab styles for
 * the views_styles module.
 *
 * @return array
 *   An array of key => value pairs suitable for inclusion as the #options in a
 *   select or radios form element. Each key must be the location of a css
 *   file for a quick tabs style. Each value should be the name of the style.
 */
function views_styles_views_styles_tabstyles() {
  $tabstyles_directory = drupal_get_path('module', 'views_styles') .'/tabstyles';
  $files = file_scan_directory($tabstyles_directory, '\.css$');

  $tabstyles = array();
  foreach ($files as $file) {
    // Skip RTL files.
    if (!strpos($file->name, '-rtl')) {
      $tabstyles[$file->filename] = drupal_ucfirst($file->name);
    }
  }
  return $tabstyles;
}

/**
 * Get a list of all available tab styles, suitable for use as an options array.
 */
function views_styles_style_options($include_defaults = TRUE) {
  $styles = module_invoke_all('views_styles_tabstyles');
  // The keys used for options must be valid html id-s.
  foreach ($styles as $style) {
    $style_options[$style] = $style;
  }
  ksort($style_options);

  if ($include_defaults) {
    $style_options = array('nostyle' => t('No style'), 'default' => t('Default style')) + $style_options;
  }

  return $style_options;
}

/**
 * Load the views_styles data.
 */
function views_styles_load($qtid, $op = 'view') {
  $views_styles = db_fetch_array(db_query('SELECT qtid, title, tabs, ajax, hide_empty_tabs, default_tab, style FROM {views_styles} WHERE qtid = %d', $qtid));
  if (!$views_styles) {
    return FALSE;
  }

  $tabs = unserialize($views_styles['tabs']);
  foreach ($tabs as $key => $tab) {
    $weight[$key] = $tab['weight'];
    if ($tab['type'] == 'qtabs' && $tab['qtid'] == $qtid) {
      unset($tabs[$key]);
      unset($weight[$key]);
    }
  }
  array_multisort($weight, SORT_ASC, $tabs);

  $views_styles['tabs'] = $tabs;
  drupal_alter('views_styles', $views_styles, $op);
  return $views_styles;
}

/**
 * Render views_styles tabpage.
 */
function views_styles_render_tabpage($tab, $hide_empty = FALSE) {
  static $cache;

  $cachekey = md5(serialize($tab));
  if (isset($cache[$cachekey])) {
    return $cache[$cachekey];
  }

  $output = '';
  switch ($tab['type']) {
    case 'qtabs':
      if (isset($tab['qtid'])) {
        if ($views_styles = views_styles_load($tab['qtid'])) {
          $output = theme('views_styles', $views_styles);
        }
      }
      break;

    case 'view':
      if (isset($tab['vid'])) {
        if (module_exists('views')) {
          if ($view = views_get_view($tab['vid'])) {
            if ($view->access($tab['display'])) {
              $view->set_display($tab['display']);
              $view->set_arguments($tab['args']);
              $view_output = $view->preview();
              if (!empty($view->result) || $view->display_handler->get_option('empty') || !empty($view->style_plugin->definition['even empty'])) {
                $output = $view_output;
              }
              else {
                $output = '';
              }
            }
            elseif (!$hide_empty) {
              $output = theme('views_styles_tab_access_denied', $tab);
            }
            $view->destroy();
          }
        }
        elseif (!$hide_empty) {
          $output = t('Views module not enabled, cannot display tab content.');
        }
      }
      break;

    case 'block':
      if (isset($tab['bid'])) {
        $pos = strpos($tab['bid'], '_delta_');
        $blockmodule = substr($tab['bid'], 0, $pos);
        $blockdelta = substr($tab['bid'], $pos + 7);
        $block = (object) module_invoke($blockmodule, 'block', 'view', $blockdelta);
        if (isset($block->content)) {
          $block->module = $blockmodule;
          $block->delta = $blockdelta;
          $block->region = 'views_styles_tabpage';
          if ($tab['hide_title'] || !isset($block->subject)) {
            $block->subject = FALSE;
          }
          $output = theme('block', $block);
        }
      }
      break;

    case 'node':
      if (isset($tab['nid'])) {
        $node = node_load($tab['nid']);
        if (!empty($node)) {
          if (node_access('view', $node)) {
            $output = node_view($node, $tab['teaser'], $tab['hide_title'], TRUE);
          }
          elseif (!$hide_empty) {
            $output = theme('views_styles_tab_access_denied', $tab);
          }
        }
      }
      break;

    case 'freetext':
      $output = $tab['text'];
      break;
  }

  $cache[$cachekey] = $output;

  return $output;
}

/**
 * Helper function to determine active tab from the url.
 */
function _views_styles_get_active_tab($views_styles) {
  $active_tab = isset($views_styles['default_tab']) ? $views_styles['default_tab'] : key($views_styles['tabs']);
  $active_tab = isset($_GET['views_styles_'. $views_styles['qtid']]) ? $_GET['views_styles_'. $views_styles['qtid']] : $active_tab;
  if (isset($active_tab) && isset($views_styles['tabs'][$active_tab])) {
    return $active_tab;
  }
  return NULL;
}

/**
 * Helper function to add views settings to ajax tabs.
 */
function _views_styles_prepare_views($tabs) {
  if (module_exists('views')) {
    views_add_js('ajax_view');
    views_add_css('views');
    foreach ($tabs as $key => $tab) {
      if ($tab['type'] == 'view') {
        // We need to pass view details to js in case there is ajax paging.
        $settings = array(
          'views' => array(
            'ajax_path' => url('views/ajax'),
            'ajaxViews' => array(
              array(
                'view_name' => $tab['vid'],
                'view_display_id' => $tab['display'],
                'view_args' => implode('/', $tab['args']),
                'view_path' => $_GET['q'],
              ),
            ),
          ),
        );
        drupal_add_js($settings, 'setting');
      }
    }
  }
}


/**
 * Helper function to use view arguments from the URL.
 */
function _views_styles_prepare_views_args($tabs) {
  foreach ($tabs as $key => $tab) {
    if ($tab['type'] == 'view') {
      $url_args = arg();
      $args = $tab['args'];

      foreach ($url_args as $id => $arg) {
        $args = str_replace("%$id", $arg, $args);
      }
      $args = preg_replace(',/?(%\d),', '', $args);
      $args = $args ? explode('/', $args) : array();

      $tab['args'] = $args;
      $tabs[$key] = $tab;
    }
  }
  return $tabs;
}

/**
 * Implementation of hook_locale().
 */
function views_styles_locale($op = 'groups', $group = NULL) {
  switch ($op) {
    case 'groups':
      return array('views_styles' => t('Quick Tabs'));

    case 'info':
      $info['views_styles']['refresh callback'] = 'views_styles_locale_refresh';
      $info['views_styles']['format'] = FALSE;
      return $info;
  }
}

/**
 * Refresh locale strings.
 */
function views_styles_locale_refresh() {
  $result = db_query("SELECT qtid FROM {views_styles}");
  while ($row = db_fetch_object($result)) {
    $views_styles = views_styles_load($row->qtid, 'locale refresh');
    foreach ($views_styles['tabs'] as $tabkey => $tab) {
      tt("views_styles:tab:$views_styles[qtid]--$tabkey:title", $tab['title'], NULL, TRUE);
    }
  }
  return TRUE;
}

/**
 * This function is based on the ctools_ajax_render() function in the CTools
 * AJAX framework. It allows us to pull js/css files and js settings for content
 * that will be loaded via ajax.
 */
function views_styles_ajax_js_css() {
  $query_string = '?'. substr(variable_get('css_js_query_string', '0'), 0, 1);
  $scripts = drupal_add_js(NULL, NULL);
  foreach ($scripts as $type => $data) {
    switch ($type) {
      case 'setting':
        $settings = $data;
        break;
      case 'inline':
        // currently we ignore inline javascript.
        break;
      default:
        // If JS preprocessing is off, we still need to output the scripts.
        // Additionally, go through any remaining scripts if JS preprocessing is on and output the non-cached ones.
        foreach ($data as $path => $info) {
          $js_files[] = base_path() . $path . ($info['cache'] ? $query_string : '?' . time());
        }
    }
  }

  $css = drupal_add_css();

  foreach ($css as $media => $types) {
    // If CSS preprocessing is off, we still need to output the styles.
    // Additionally, go through any remaining styles if CSS preprocessing is on and output the non-cached ones.
    foreach ($types as $type => $files) {
      if ($type == 'module') {
        // Setup theme overrides for module styles.
        $theme_styles = array();
        foreach (array_keys($css[$media]['theme']) as $theme_style) {
          $theme_styles[] = basename($theme_style);
        }
      }
      foreach ($types[$type] as $file => $preprocess) {
        // If the theme supplies its own style using the name of the module style, skip its inclusion.
        // This includes any RTL styles associated with its main LTR counterpart.
        if ($type == 'module' && in_array(str_replace('-rtl.css', '.css', basename($file)), $theme_styles)) {
          // Unset the file to prevent its inclusion when CSS aggregation is enabled.
          unset($types[$type][$file]);
          continue;
        }
        // Only include the stylesheet if it exists.
        if (file_exists($file)) {
          $css_files[] = array(
            'file' => base_path() . $file . $query_string,
            'media' => $media,
          );
        }
      }
    }
  }

  $js_css = array();
  if (!empty($settings)) {
    $js_css['js_settings'] = call_user_func_array('array_merge_recursive', $settings);
  }
  if (!empty($js_files)) {
    $js_css['js_files'] = $js_files;
  }
  if (!empty($css_files)) {
    $js_css['css_files'] = $css_files;
  }
  return $js_css;
}

/**
 * Implementation of hook_views_api().
 */
function views_styles_views_api() {
  return array(
    'api' => 2,
    'path' => drupal_get_path('module', 'views_styles') .'/includes',
  );
}

/**
 * PHP4 version of array_fill_keys().
 */
function views_styles_array_fill_keys($keys, $value){
  $return = array();
  foreach ($keys as $key) {
    $return[$key] = $value;
  }
  return $return;
}
