Drupal.views_stylesShowHide = function() {
  $(this).parents('tr').find('td.qt-tab-' + this.value + '-content').show().siblings('td.qt-tab-content').hide();
};

Drupal.behaviors.views_stylesform = function(context) {
  $('#views_styles-form tr').not('.views_styles-form-processed').addClass('views_styles-form-processed').each(function(){
    var currentRow = $(this);
    currentRow.find('div.form-item :input[name*="type"]').bind('click', Drupal.views_stylesShowHide);
    $(':input[name*="type"]:checked', this).trigger('click');
  })
};