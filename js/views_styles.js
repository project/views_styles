
Drupal.settings.views = Drupal.settings.views || {'ajax_path': 'views/ajax'};

Drupal.behaviors.views_styles = function (context) {
  $('.views_styles_wrapper:not(.views_styles-processed)', context).addClass('views_styles-processed').each(function(){
    Drupal.views_styles.prepare(this);
  });
};

Drupal.views_styles = Drupal.views_styles || {};
Drupal.views_styles.ajax = {};
Drupal.views_styles.scripts = {};
Drupal.views_styles.css = {};

// setting up the inital behaviours
Drupal.views_styles.prepare = function(el) {
  var i = 0;
  // el.id format: "views_styles-$qtid"
  var qtid = el.id.substring(el.id.indexOf('-') +1);

  $(el).find('ul.views_styles_tabs li a').each(function(){
    this.myTabIndex = i++;
    this.qtid = qtid;
    $(this).unbind('click').bind('click', views_stylesClick);
  });

  // Search for the active tab.
  var $active_tab = $(el).children('.views_styles_tabs').find('li.active a');

  if ($active_tab.hasClass('qt_tab') || $active_tab.hasClass('qt_ajax_tab')) {
    $active_tab.trigger('click');
  }
  else {
    // Click on the first tab.
    $(el).children('.views_styles_tabs').find('li.first a').trigger('click');
  }
  return false;
}

// constructor for an individual tab
Drupal.views_styles.tab = function (el) {
  this.element = el;
  this.tabIndex = el.myTabIndex;
  this.qtid = el.qtid;
  var qtKey = 'qt_' + this.qtid;
  var i = 0;
  for (var key in Drupal.settings.views_styles[qtKey].tabs) {
    if (i == this.tabIndex) {
      this.tabObj = Drupal.settings.views_styles[qtKey].tabs[key];
      this.tabKey = key;
    }
    i++;
  }
  this.tabpage_id = 'views_styles_tabpage_' + this.qtid + '_' + this.tabKey;
  this.container = $('#views_styles_container_' + this.qtid);
  this.tabpage = this.container.find('#' + this.tabpage_id);
  // The 'this' variable will not persist inside of the options object.
  var tab = this;
  this.options = {
    success: function(response) {
      return tab.success(response);
    },
    complete: function(response) {
      return tab.complete();
    }
  }
}

// ajax callback for non-views tabs
Drupal.views_styles.tab.prototype.success = function(response) {
  this.container.append(Drupal.theme('views_stylesResponse', this, response.data.content));
  $.extend(true, Drupal.settings, response.data.js_css.js_settings);
  Drupal.views_styles.ajax.scripts(response.data.js_css.js_files);
  Drupal.views_styles.ajax.css_files(response.data.js_css.css_files);
  Drupal.attachBehaviors(this.container);
}

// function to call on completed ajax call
// for non-views tabs
Drupal.views_styles.tab.prototype.complete = function() {
  // stop the progress bar
  this.stopProgress();
}


Drupal.views_styles.tab.prototype.stopProgress = function () {
  if (this.progress.element) {
    $(this.progress.element).remove();
  }
  $(this.element).removeClass('progress-disabled').attr('disabled', false);
}

Drupal.views_styles.tab.prototype.startProgress = function () {
  var progressBar = new Drupal.progressBar('qt-progress-' + this.element.id, null, null, null);
  progressBar.setProgress(-1, Drupal.t('Loading'));
  this.progress = {};
  this.progress.element = $(progressBar.element).addClass('qt-progress qt-progress-bar');
  this.container.prepend(this.progress.element);
}

Drupal.views_styles.tab.prototype.views_stylesAjaxView = function() {
  // Create an empty div for the tabpage. The generated view will be inserted into this.
  var tab = this;
  tab.container.append(Drupal.theme('views_stylesResponse', this, null));

  var target;
  target = $('#' + tab.tabpage_id + ' > div');
  var ajax_path = Drupal.settings.views.ajax_path;
   //If there are multiple views this might've ended up showing up multiple times.
  if (ajax_path.constructor.toString().indexOf("Array") != -1) {
    ajax_path = ajax_path[0];
  }
  var args;
  if (tab.tabObj.args != '') {
    args = tab.tabObj.args.join('/');
  } else {
    args = '';
  }
  var viewData = {
    'view_name': tab.tabObj.vid,
    'view_display_id': tab.tabObj.display,
    'view_args': args
  }
  $.ajax({
    url: ajax_path,
    type: 'GET',
    data: viewData,
    success: function(response) {
      // Call all callbacks.
      if (response.__callbacks) {
        $.each(response.__callbacks, function(i, callback) {
          eval(callback)(target, response);
        });
      }
    },
    complete: function() {
      tab.stopProgress();
    },
    error: function() { alert(Drupal.t("An error occurred at @path.", {'@path': ajax_path})); },
    dataType: 'json'
  });
}

var views_stylesClick = function() {

  var tab = new Drupal.views_styles.tab(this);

  // Set clicked tab to active.
  $(this).parents('li').siblings().removeClass('active');
  $(this).parents('li').addClass('active');

  // Hide all tabpages.
  tab.container.children().addClass('views_styles-hide');

  // Show the active tabpage.
  if (tab.tabpage.hasClass('views_styles_tabpage')) {
    tab.tabpage.removeClass('views_styles-hide');
  }
  else {
    if ($(this).hasClass('qt_ajax_tab')) {
      tab.startProgress();
      // Construct the ajax tabpage.
      if (tab.tabObj.type != 'view') {
        // construct the ajax path to retrieve the content, depending on type
        var qtAjaxPath = Drupal.settings.basePath + 'views_styles/ajax/' + tab.tabObj.type + '/';
        switch (tab.tabObj.type) {
          case 'node':
            qtAjaxPath +=  tab.tabObj.nid + '/' + tab.tabObj.teaser + '/' + tab.tabObj.hide_title;
            break;
          case 'block':
            qtAjaxPath +=  tab.tabObj.bid + '/' + tab.tabObj.hide_title;
            break;
          case 'qtabs':
            qtAjaxPath +=  tab.tabObj.qtid;
            break;
        }
        
        $.ajax({
          url: qtAjaxPath,
          type: 'GET',
          data: null,
          success: tab.options.success,
          complete: tab.options.complete,
          dataType: 'json'
        });
      }
      else {
        // special treatment for views
        tab.views_stylesAjaxView();
      }
    }
  }
  return false;
}

// theme function for ajax response
Drupal.theme.prototype.views_stylesResponse = function(tab, content) {
  var newDiv = tab.tabObj.type == 'view' ? '<div id="' + tab.tabpage_id + '" class="views_styles_tabpage"><div></div></div>' : '<div id="' + tab.tabpage_id + '" class="views_styles_tabpage">' + content + '</div>';
  return newDiv;
};


/**
 * Quicktabs' implementation of merlinofchaos's CTools js magic for loading
 * required js files for ajax-loaded content
 */
Drupal.views_styles.ajax.scripts = function(files) {
  // Build a list of scripts already loaded:
  $('script').each(function () {
    Drupal.views_styles.scripts[$(this).attr('src')] = $(this).attr('src');
  });

  var html = '';
  for (i in files) {
    if (!Drupal.views_styles.scripts[files[i]]) {
      Drupal.views_styles.scripts[files[i]] = files[i];
      html += '<script type="text/javascript" src="' + files[i] + '"></script>';
    }
  }

  if (html) {
    $('body').append($(html));
  }
};

/**
 * Quicktabs' implementation of merlinofchaos's CTools js magic for loading
 * required css files for ajax-loaded content
 */
Drupal.views_styles.ajax.css_files = function(files) {
  // Build a list of css files already loaded:

  $('link:not(.qt-temporary-css)').each(function () {
    if ($(this).attr('type') == 'text/css') {
      Drupal.views_styles.css[$(this).attr('href')] = $(this).attr('href');
    }
  });

  var html = '';
  for (i in files) {
    if (!Drupal.views_styles.css[files[i].file]) {
      html += '<link class="qt-temporary-css" type="text/css" rel="stylesheet" media="' + files[i].media +
        '" href="' + files[i].file + '" />';
    }
  }

  if (html) {
    $('link.ctools-temporary-css').remove();
    $('body').append($(html));
  }
};
